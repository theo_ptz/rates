describe('Tests', function () {
    var assert = require('assert');
    var rates = require('../');

    it('Should convert usd to php', function (done) {
        rates.convert(1, 'usd', 'php', function (err, value) {
            assert.strictEqual(err, null);
            assert.strictEqual(typeof value, 'number');

            done();
        });
    });
});