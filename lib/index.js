// finance converter url
var url = 'https://www.google.com/finance/converter';

var qs = require('querystring');
var request = require('request');
var _ = require('lodash');

exports.convert = function (amount, from, to, cb) {
    var query = qs.stringify({
        a: amount,
        from: from,
        to: to
    });

    request({
        url: url + '?' + query,
        method: 'GET',
        timeout: 15000
    }, function (err, response, body) {
        if (err) {
            return cb(err);
        }

        if (response.statusCode !== 200 || !body) {
            return cb(new Error('INCORRECT_RESPONSE'));
        }

        var re = new RegExp('\<span class=bld\>([0-9]+(?:[\.]{1}[0-9]+)) ' + to.toUpperCase() + '\<\/span\>');
        var parts = re.exec(body);

        if (!_.isArray(parts)) {
            return cb(new Error('WRONG_RESPONSE'));
        }

        var value = parseFloat(parts[1]);
        if (isNaN(value)) {
            return cb(new Error('WRONG_RESPONSE'));
        }

        cb(null, value);
    });
};